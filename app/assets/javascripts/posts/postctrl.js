angular.module('flapperNews')
    .controller('PostsCtrl', [
        '$scope',
        '$stateParams',
        'posts',
        function ($scope, $stateParams, posts) {
            var post = posts.get($stateParams.id);
            post.then(
                function (answer) {
                    $scope.post = answer
                },
                function (error) {
                    console.log(error);
                },
                function (progress) {
                    console.log(progress);
                });
            
            
            $scope.addComment = function () {
                if ($scope.body === '') { return; }
                posts.addComment($scope.post.id, {
                    body: $scope.body,
                    author: 'user',
                }).success(function (comment) {
                    $scope.post.comments.push(comment);
                });
                $scope.body = '';
            };
            $scope.incrementUpvotes = function (comment) {
                posts.upvoteComment(post, comment);
            };

        }]);
